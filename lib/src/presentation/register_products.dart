// ignore_for_file: sized_box_for_whitespace, avoid_unnecessary_containers

import 'package:crud_isar_db/src/data/isar/product.dart';
import 'package:flutter/material.dart';

import '../data/class/isar_product.dart';

class RegisterProducts extends StatefulWidget {

  const RegisterProducts({super.key});

  @override
  State<RegisterProducts> createState() => _RegisterProductsState();
}

class _RegisterProductsState extends State<RegisterProducts> {
  final _formkey = GlobalKey<FormState>();

  final product = TextEditingController();

  final description = TextEditingController();

  String productext = '';

  String descripciontext = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Registrar producto')),
      body: Form(
          key: _formkey,
          child: Padding(
            padding: const EdgeInsets.all(40.0),
            child: Column(children: [
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                          child: TextFormField(
                              controller: product,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'no hay datos';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: EdgeInsets.all(15.0),
                                  labelText: 'Nombre Producto'
                              )
                          )
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: TextFormField(
                              maxLines: 8,
                              controller: description,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'no hay datos';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: EdgeInsets.all(15.0),
                                  labelText: 'Descripcion Producto'
                              )
                            )
                          ),
                    ],
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: 50.0,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(10.0))),
                            onPressed: () async {
                              if (_formkey.currentState!.validate()) {
                                productext = product.text;
                                descripciontext = description.text;

                                IsarProduct().saveproduct(Product(
                                  name: productext,
                                  description: descripciontext
                                ));
                              }
                            },
                            child: const Text(
                              'Crear Productos',
                              style: TextStyle(fontSize: 18.0),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              )
            ]),
          )),
    );
  }
}
