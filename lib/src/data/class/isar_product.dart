import 'package:crud_isar_db/src/services/isar_db.dart';
import 'package:isar/isar.dart';

import '../isar/product.dart';

class IsarProduct {
  final isar = IsarServices.isar;
  //Guardar producto
  Future<void> saveproduct(Product produc) async {
    final saveproduct = isar.products;
    await isar.writeTxn(() async {
      await saveproduct.put(produc);
    });
  }

  //Obtener toda la lista de productos
  Future<List<Product>> getall() async {
    return await isar.products.where().findAll();
  }

  //obtiene datos del producto con el id
  Future<Product?> getproduct(id) async {
    return isar.products.getSync(id);
  }

  //actulizar los datos del producto
  Future<void> updateproduct(Product update) async {
    await isar.writeTxn(() async {
      await isar.products.put(update);
    });
  }

  Future <void> deleteproduct(id)async{
    await isar.writeTxn(() async {
  final success = await isar.products.delete(id);
  print('Recipe deleted: $success');
});

  }
}
