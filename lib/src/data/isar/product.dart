import 'package:isar/isar.dart';
part 'product.g.dart';

@collection
class Product {
  Id? id = Isar.autoIncrement;
  String? name;
  String? description;

  Product({this.id, this.name, this.description});

  Product.empty();
}
