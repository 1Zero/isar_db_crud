import 'package:crud_isar_db/src/data/isar/product.dart';
import 'package:isar/isar.dart';

class IsarServices {
  static late Isar isar;

  static Future<void> isarinitializer() async {
    isar = await Isar.open([ProductSchema], directory: '');
  }
}
