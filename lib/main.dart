import 'package:crud_isar_db/src/data/isar/product.dart';
import 'package:crud_isar_db/src/presentation/register_products.dart';
import 'package:crud_isar_db/src/services/isar_db.dart';
import 'package:flutter/material.dart';

import 'src/data/class/isar_product.dart';

void main() async {
  await IsarServices.isarinitializer();
  runApp(const Crud());
}

class Crud extends StatelessWidget {
  const Crud({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<List<Product>>? _future;

  @override
  void initState() {
    super.initState();
    actualizarVista();
  }

  void actualizarVista() {
    setState(() {
      _future = IsarProduct().getall();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: FutureBuilder<List<Product>>(
          future: _future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<Product>? data = snapshot.data;
              return Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: ListView.builder(
                      itemCount: data!.length,
                      itemBuilder: (context, index) {
                        Product product = data[index];
                        return Dismissible(
                          key: UniqueKey(),
                          direction: DismissDirection.startToEnd,
                          background: Container(
                            color: Colors.red,
                            padding: const EdgeInsets.only(left: 5),
                            child: const Align(
                              alignment: Alignment.centerLeft,
                              child: Icon(Icons.delete),
                            ),
                          ),
                          onDismissed: (direction) {
                            print(direction);
                            print(product.id);
                            IsarProduct().deleteproduct(product.id);
                            setState(() {
                              data.removeAt(index);
                            });
                            
                            //actualizarVista();
                          },
                          child: ListTile(
                            leading: const Icon(Icons.person),
                            title: Text('${product.name}'),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              );
            }
            return const CircularProgressIndicator();
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context)
              .push(
            MaterialPageRoute(builder: (context) => const RegisterProducts()),
          )
              .then((_) {
            actualizarVista();
          });
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
